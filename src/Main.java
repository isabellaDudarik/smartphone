import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Shop[] shops = Generate.generate();

        for (Shop shop : shops) {
            Smartphone smartphone = shop.searchForTheCheapestAmartphone();
            if (smartphone != null) {
                Shop searchByShops = shops[0];
                if (searchByShops.isCheapestInTheShops(shop)) {
                    searchByShops = shop;
                    System.out.println(String.format("The cheapest smartphone in the shop: %s. This is: %s",
                            searchByShops.getName(),
                            smartphone.getInfo()
                    ));
                }
            }
        }

        Scanner in = new Scanner(System.in);
        System.out.println("Enter manufacturer name: ");
        String manufacturer = in.nextLine();

        for (Shop shop : shops) {
            Smartphone smartphone = shop.manufacturerSmartphone(manufacturer);
            if (smartphone != null) {
                System.out.println(String.format("%s has smartphone %s ",
                        shop.getName(), smartphone.getInfo()));
            }
        }
        System.out.println(" ");

        for (Shop shop : shops) {
            Smartphone smartphone = shop.smartphoneWithTheLargestDiagonal();
            if (smartphone != null) {
                Shop searchByShops = shops[0];
                if (searchByShops.isCheapestInTheShops(shop)) {
                    searchByShops = shop;
                    System.out.println(String.format("The cheapest smartphone with the largest diagonal: %s   in the shop %s",
                            smartphone.getInfo(), searchByShops.getName()));
                }
            }
        }
    }
}
