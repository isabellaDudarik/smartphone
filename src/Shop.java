public class Shop {
    private String name;
    private String address;
    private Smartphone[] smartphones;

    public Shop(String name, String address, Smartphone[] smartphones) {
        this.name = name;
        this.address = address;
        this.smartphones = smartphones;
    }

    public String getName() {
        return name;
    }

    public Smartphone[] getSmartphones() {
        return smartphones;
    }

    public Smartphone searchForTheCheapestAmartphone() {
        Smartphone cheapestSmartphone = smartphones[0];
        for (int i = 1; i < smartphones.length; i++) {
            if (cheapestSmartphone.isCheapestThen(smartphones[i])) {
                cheapestSmartphone = smartphones[i];
            }
        }
        return
                cheapestSmartphone;
    }

    public boolean isCheapestInTheShops(Shop shop) {
        return searchForTheCheapestAmartphone().getPrice() > shop.searchForTheCheapestAmartphone().getPrice();
    }


    public Smartphone manufacturerSmartphone(String manufacturer) {
        for (Smartphone smartphone : smartphones) {
            if (smartphone.getManufacturer().equalsIgnoreCase(manufacturer)) {
                return smartphone;
            }
        }
        return null;
    }

    public Smartphone smartphoneWithTheLargestDiagonal() {
        Smartphone theLargestSmartphone = smartphones[0];
        for (Smartphone smartphone : smartphones) {
            if (theLargestSmartphone.isLargestThen(smartphone)) {
                theLargestSmartphone = smartphone;
            }
        }
        return theLargestSmartphone;
    }
}
