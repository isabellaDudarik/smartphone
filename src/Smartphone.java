public class Smartphone {
    private String manufacturer;
    private float displayDiagonal;
    private int price;

    public Smartphone(String manufacturer, float displayDiagonal, int price) {
        this.manufacturer = manufacturer;
        this.displayDiagonal = displayDiagonal;
        this.price = price;
    }

    public Smartphone() {

    }

    public String getManufacturer() {
        return manufacturer;
    }

    public float getDisplayDiagonal() {
        return displayDiagonal;
    }

    public int getPrice() {
        return price;
    }

    public String getInfo() {
        return String.format("Manufacturer: %s, Display diagonal: %s, price: %d", manufacturer, displayDiagonal, price);
    }

    public boolean isCheapestThen(Smartphone smartphone) {
        return price > smartphone.price;
    }

    public boolean isLargestThen(Smartphone smartphone) {
        return displayDiagonal < smartphone.displayDiagonal;
    }


}

