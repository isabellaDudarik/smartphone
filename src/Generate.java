public class Generate {
    public static Shop[] generate() {
        Shop[] shops = new Shop[4];
        {
            Smartphone[] smartphones = new Smartphone[5];
            smartphones[0] = new Smartphone("Apple", 4.7f, 11995);
            smartphones[1] = new Smartphone("Apple", 5.5f, 30250);
            smartphones[2] = new Smartphone("Samsung", 6f, 7999);
            smartphones[3] = new Smartphone("Xiaomi", 5.5f, 5499);
            smartphones[4] = new Smartphone("Huawei", 6f, 32999);

            shops[0] = new Shop("Citrus", "Pushkinska st.", smartphones);
        }
        {
            Smartphone[] smartphones = new Smartphone[3];
            smartphones[0] = new Smartphone("Samsung", 6f, 7600);
            smartphones[1] = new Smartphone("Xiaomi", 5.5f, 5000);
            smartphones[2] = new Smartphone("Huawei", 6f, 31000);

            shops[1] = new Shop("Allo", "Sumska st.", smartphones);
        }
        {
            Smartphone[] smartphones = new Smartphone[2];
            smartphones[0] = new Smartphone("Apple", 4.7f, 10200);
            smartphones[1] = new Smartphone("Apple", 5.5f, 29400);

            shops[2] = new Shop("Apple Shop", "Alchevskih st.", smartphones);
        }
        {
            Smartphone[] smartphones = new Smartphone[4];
            smartphones[0] = new Smartphone("Apple", 4.7f, 13199);
            smartphones[1] = new Smartphone("Samsung", 6f, 8399);
            smartphones[2] = new Smartphone("Xiaomi", 5.5f, 5999);
            smartphones[3] = new Smartphone("Huawei", 6f, 34300);

            shops[3] = new Shop("Cactus", "Bluhera st.", smartphones);
        }
        return shops;
    }
}
